<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
	  private $rezorpayId = "rzp_test_ItrThxVNDFSX6t";
   	private $rezorpayKey = "cuIkI4STqmYBW3RtniLjYuAd";

   public function index(Request $request){

      $receiptId = Str::random(20);

      $api = new Api($this->rezorpayId, $this->rezorpayKey);

          $order = $api->order->create(array(
            'receipt' => $receiptId,
            'amount' => $request->all()['amount'] * 100,
            'currency' => 'INR'
            )
        );


          $response = [

          	'orderId' => $order['id'],
          	'rezorpayId' => $this->rezorpayId,
          	'amount' => $request->all()['amount'] * 100,
          	'name' => $request->all()['name'],
          	'currency' => 'INR',
          	'email' => $request->all()['email'],
          	'contactNumber'=> $request->all()['contactNumber'],
          	'address'=> $request->all()['address'],
          	'description' => 'Testing  description',

          ];

          return view('payment_checkout',compact('response'));
   	  
   }

   
}
